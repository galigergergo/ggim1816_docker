-- docker-compose exec -T tour-db /usr/local/bin/psql tourdb tourdbuser <misc/populate.sql
INSERT INTO countries (name, population)
VALUES ('Romania', 19);
INSERT INTO tours (name, region, country_id, distance, duration, ascent, descent, peak)
VALUES ('Tour1', 'Transylvania', 1, 123.1, 0.5, 100, 40, 750);
