-- docker-compose exec -T tour-db /usr/local/bin/psql tourdb tourdbuser <misc/setup.sql
DROP TABLE IF EXISTS tours;
DROP TABLE IF EXISTS countries;
CREATE TABLE countries
(
    id          integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name        varchar(64),
    population  integer
);
CREATE TABLE tours
(
    id          integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name        varchar(64),
    region      varchar(64),
    country_id  integer,
    distance    real,
    duration    real,
    ascent      integer,
    descent     integer,
    peak        integer,
    CONSTRAINT fk_country
          FOREIGN KEY(country_id)
    	  REFERENCES countries(id)
);