FROM csabasulyok/gradle:6.6.1-jdk8-alpine AS build.env
WORKDIR /usr/ggim1816
COPY . ./
RUN gradle war

FROM tomcat:9.0.20-jre8-alpine
RUN rm -r ./webapps/ROOT
COPY --from=build.env  /usr/ggim1816/ggim1816-web/build/libs/ggim1816-web-1.0-SNAPSHOT.war ./webapps/ROOT.war
COPY --from=build.env  /usr/ggim1816/ggim1816-web/build/libs/ggim1816-web-1.0-SNAPSHOT.war ./webapps/temp.war
COPY tomcat.sh ./webapps/
CMD ["sh", "./webapps/tomcat.sh"]