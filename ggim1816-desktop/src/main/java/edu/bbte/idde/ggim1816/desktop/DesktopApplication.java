package edu.bbte.idde.ggim1816.desktop;

import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;
import edu.bbte.idde.ggim1816.backend.model.Country;
import edu.bbte.idde.ggim1816.backend.model.Tour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.Collection;

public class DesktopApplication extends JPanel {
    /** table */
    JTable table;

    /** table refresh button */
    JButton refreshButton;

    static final TourDao TOUR_MEMORY_DAO = DaoFactory.getInstance().getTourDao();

    static final Logger LOGGER = LoggerFactory.getLogger(DesktopApplication.class);

    /**
     * constructs new instance of the client program pane
     */
    public DesktopApplication() {
        super();

        // fill the table memory dao
        TOUR_MEMORY_DAO.create(new Tour("Tour1", "northern", new Country("Romania", 19), 11.1, 3.5, 200, 30, 873));
        TOUR_MEMORY_DAO.create(new Tour("Tour2", "southern", new Country("France", 30), 12.0, 4.0, 257, 45, 1111));
        TOUR_MEMORY_DAO.create(new Tour("Tour3", "western", new Country("Germany", 40), 1.1, 3.5, 200, 243, 675));
        TOUR_MEMORY_DAO.create(new Tour("Tour4", "northern", new Country("Norway", 24), 5.1, 2.0, 333, 111, 435));
        TOUR_MEMORY_DAO.create(new Tour("Tour5", "central", new Country("India", 1000), 11.6, 4.0, 355, 145, 866));

        // Create individual GUI elements
        MyTableModel model = new MyTableModel();
        table = new JTable(model);
        refreshButton = new JButton("Refresh");

        // Configure button
        refreshButton.setActionCommand("refresh");
        refreshButton.addActionListener(e -> {
            Object[][] data = configureButton();
            model.setData(data);
        });

        // Define a container and add elements to it
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(table, BorderLayout.CENTER);
        panel.add(refreshButton, BorderLayout.SOUTH);

        add(panel);
    }

    /**
     * Configure refresh button to add all the tours to the table
     */
    private Object[][] configureButton() {
        LOGGER.info("Refresh button pressed.");
        Collection<Tour> tours = TOUR_MEMORY_DAO.selectAll();
        Object[][] data = new Object[tours.size()][7];
        int counter = 0;
        for (Tour tour : tours) {
            data[counter][0] = tour.getName();
            data[counter][1] = tour.getRegion();
            data[counter][2] = tour.getDistance();
            data[counter][3] = tour.getDuration();
            data[counter][4] = tour.getAscent();
            data[counter][5] = tour.getDescent();
            data[counter][6] = tour.getPeak();
            counter++;
        }
        return data;
    }

    /**
     * TableModel of the JTable
     */
    static class MyTableModel extends AbstractTableModel {
        private final String[] columnNames = { "Name", "Region", "Distance",
                "Duration", "Ascent", "Descent", "Peak" };

        private Object[][] data = {{"", "", "", "", "", "", ""}};

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return data.length;
        }

        @Override
        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        public void setData(Object[]... obj) {
            if (obj == null) {
                this.data = new Object[0][7];
            } else {
                this.data = new Object[obj.length][obj[0].length];
                for (int i = 0; i < obj.length; i++) {
                    for (int j = 0; j < obj[i].length; j++) {
                        this.data[i][j] = obj[i][j];
                    }
                }
            }
            fireTableDataChanged();
        }
    }

    /**
     * Create the GUI and show it.
     */
    private static void createAndShowGUI() {
        // Create and set up a frame window
        JFrame frame = new JFrame("Client");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(720, 480));

        // Create new ClientProgram pane
        DesktopApplication pane = new DesktopApplication();
        frame.setContentPane(pane);

        // Display the window
        frame.pack();
        frame.setVisible(true);

        LOGGER.info("Application window displayed.");
    }

    /**
     * main
     */
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(DesktopApplication::createAndShowGUI);
    }
}
