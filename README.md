# IDDE laborfeladatok

 - SETUP POSTGRESQL DATABASE: 
    - docker-compose exec -T tour-db /usr/local/bin/psql tourdb tourdbuser <misc/setup.sql
 - POPULATE POSTGRESQL DATABASE: 
    - docker-compose exec -T tour-db /usr/local/bin/psql tourdb tourdbuser <misc/populate.sql