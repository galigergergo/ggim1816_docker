package edu.bbte.idde.ggim1816.backend.dao.jdbc;

import edu.bbte.idde.ggim1816.backend.dao.DaoException;
import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.jdbc.config.DataSourceFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;
import edu.bbte.idde.ggim1816.backend.model.Tour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TourJdbcDao extends JdbcDao<Tour> implements TourDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(TourJdbcDao.class);

    @Override
    public int create(Tour tour) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            int countryId = DaoFactory.getInstance().getCountryDao().create(tour.getCountry());
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO tours (name, region, country_id, distance, duration, ascent, descent, peak) "
                            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            prepareStatementFromEntity(statement, tour, countryId);
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                LOGGER.info("Created tour with id: {} (JDBC)", generatedKeys.getInt(1));
                return generatedKeys.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error("Could not create tour! (JDBC)", e);
            throw new DaoException("Could not create tour! (JDBC)", e);
        }
        return -1;
    }

    @Override
    public Collection<Tour> selectAll() {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(
                    "SELECT id, name, region, country_id, distance, duration, ascent, descent, peak FROM tours;");
            List<Tour> results = new ArrayList<>();
            while (resultSet.next()) {
                results.add(convertResultSetRow(resultSet));
            }
            return results;
        } catch (SQLException e) {
            LOGGER.error("Could not find all tours! (JDBC)", e);
            throw new DaoException("Could not find all tours! (JDBC)", e);
        }
    }

    @Override
    public Tour selectById(Integer id) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "SELECT id, name, region, country_id, distance, duration, ascent, "
                            + "descent, peak FROM tours WHERE id = ?;");
            statement.setInt(1, id);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return convertResultSetRow(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Could not find tour by id! (JDBC)", e);
            throw new DaoException("Could not find tour by id! (JDBC)", e);
        }
        return null;
    }

    @Override
    public void updateById(Integer id, Tour tour) {
        DaoFactory.getInstance().getCountryDao().updateById(tour.getCountry().getId(), tour.getCountry());
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "UPDATE tours "
                            + "SET name = ?, region = ?, country_id = ?, distance = ?, duration = ?, ascent = ?,"
                            + " descent = ?, peak = ? "
                            + "WHERE id = ?;");
            prepareStatementFromEntity(statement, id, tour);
            statement.executeUpdate();
            LOGGER.info("Updated tour with id: {} (JDBC)", id);
        } catch (SQLException e) {
            LOGGER.error("Could not update tour by id! (JDBC)", e);
            throw new DaoException("Could not update tour by id! (JDBC)", e);
        }
    }

    @Override
    public void updateNameById(Integer id, String name) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "UPDATE tours SET name = ? WHERE id = ?;");
            statement.setString(1, name);
            statement.setInt(2, id);
            statement.executeUpdate();
            LOGGER.info("Updated tour name with id: {} (JDBC)", id);
        } catch (SQLException e) {
            LOGGER.error("Could not update tour name by id! (JDBC)", e);
            throw new DaoException("Could not update tour name by id! (JDBC)", e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM tours WHERE id = ?;");
            statement.setInt(1, id);
            statement.executeUpdate();
            LOGGER.info("Deleted tour with id: {} (JDBC)", id);
        } catch (SQLException e) {
            LOGGER.error("Could not delete tours by id! (JDBC)", e);
            throw new DaoException("Could not delete tours by id! (JDBC)", e);
        }
    }

    private Tour convertResultSetRow(ResultSet resultSet) throws SQLException {
        Tour tour = new Tour();
        tour.setId(resultSet.getInt("id"));
        tour.setName(resultSet.getString("name"));
        tour.setRegion(resultSet.getString("region"));
        tour.setCountry(DaoFactory.getInstance().getCountryDao().selectById(resultSet.getInt("country_id")));
        tour.setDistance(resultSet.getDouble("distance"));
        tour.setDuration(resultSet.getDouble("duration"));
        tour.setAscent(resultSet.getInt("ascent"));
        tour.setDescent(resultSet.getInt("descent"));
        tour.setPeak(resultSet.getInt("peak"));
        return tour;
    }

    private void prepareStatementFromEntity(PreparedStatement statement, Tour tour, int countryId) throws SQLException {
        statement.setString(1, tour.getName());
        statement.setString(2, tour.getRegion());
        statement.setInt(3, countryId);
        statement.setDouble(4, tour.getDistance());
        statement.setDouble(5, tour.getDuration());
        statement.setInt(6, tour.getAscent());
        statement.setInt(7, tour.getDescent());
        statement.setInt(8, tour.getPeak());
    }

    private void prepareStatementFromEntity(PreparedStatement statement, Integer id, Tour tour) throws SQLException {
        statement.setString(1, tour.getName());
        statement.setString(2, tour.getRegion());
        statement.setInt(3, tour.getCountry().getId());
        statement.setDouble(4, tour.getDistance());
        statement.setDouble(5, tour.getDuration());
        statement.setInt(6, tour.getAscent());
        statement.setInt(7, tour.getDescent());
        statement.setInt(8, tour.getPeak());
        statement.setInt(9, id);
    }
}
