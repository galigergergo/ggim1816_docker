package edu.bbte.idde.ggim1816.backend.dao;

import edu.bbte.idde.ggim1816.backend.config.AppConfig;
import edu.bbte.idde.ggim1816.backend.config.ConfigLoader;
import edu.bbte.idde.ggim1816.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.memory.MemoryDaoFactory;

public abstract class DaoFactory {

    private static DaoFactory instance;

    public static synchronized DaoFactory getInstance() {
        if (instance == null) {
            AppConfig appConfig = ConfigLoader.get();
            if ("mem".equals(appConfig.getDaoType())) {
                instance = new MemoryDaoFactory();
            } else if ("jdbc".equals(appConfig.getDaoType())) {
                instance = new JdbcDaoFactory();
            } else {
                throw new DaoException("Dao type not supported!");
            }
        }
        return instance;
    }

    public abstract TourDao getTourDao();

    public abstract CountryDao getCountryDao();

}
