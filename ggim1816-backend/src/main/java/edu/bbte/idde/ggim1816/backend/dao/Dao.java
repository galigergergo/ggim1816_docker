package edu.bbte.idde.ggim1816.backend.dao;

import edu.bbte.idde.ggim1816.backend.model.BaseEntity;

import java.util.Collection;

public interface Dao<T extends BaseEntity> {

    int create(T entity);

    Collection<T> selectAll();

    T selectById(Integer id);

    void updateById(Integer id, T entity);

    void deleteById(Integer id);

}
