package edu.bbte.idde.ggim1816.backend.model;

/**
 * tour class
 */
public class Tour extends BaseEntity {
    private String name;
    private String region;
    private Country country;
    private Double distance;
    private Double duration;
    private Integer ascent;
    private Integer descent;
    private Integer peak;

    public Tour() {
        super();
    }

    public Tour(String name, String region, Country country, Double distance, Double duration, Integer ascent,
                Integer descent, Integer peak) {
        super();
        this.name = name;
        this.region = region;
        this.country = country;
        this.distance = distance;
        this.duration = duration;
        this.ascent = ascent;
        this.descent = descent;
        this.peak = peak;
    }

    public void setTour(Tour tour) {
        this.name = tour.name;
        this.region = tour.region;
        this.distance = tour.distance;
        this.duration = tour.duration;
        this.ascent = tour.ascent;
        this.descent = tour.descent;
        this.peak = tour.peak;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public int getAscent() {
        return ascent;
    }

    public void setAscent(Integer ascent) {
        this.ascent = ascent;
    }

    public int getDescent() {
        return descent;
    }

    public void setDescent(Integer descent) {
        this.descent = descent;
    }

    public int getPeak() {
        return peak;
    }

    public void setPeak(Integer peak) {
        this.peak = peak;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Tour{");
        sb.append("name='").append(name).append('\'');
        sb.append(", region='").append(region).append('\'');
        sb.append(", distance=").append(distance);
        sb.append(", duration=").append(duration);
        sb.append(", ascent=").append(ascent);
        sb.append(", descent=").append(descent);
        sb.append(", peak=").append(peak);
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
