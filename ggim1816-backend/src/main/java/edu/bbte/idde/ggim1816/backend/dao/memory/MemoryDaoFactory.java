package edu.bbte.idde.ggim1816.backend.dao.memory;

import edu.bbte.idde.ggim1816.backend.dao.CountryDao;
import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;

public class MemoryDaoFactory extends DaoFactory {

    private TourDao tourMemoryDao;
    private CountryDao countryMemoryDao;

    @Override
    public TourDao getTourDao() {
        if (tourMemoryDao == null) {
            tourMemoryDao = new TourMemoryDao();
        }
        return tourMemoryDao;
    }

    @Override
    public CountryDao getCountryDao() {
        if (countryMemoryDao == null) {
            countryMemoryDao = new CountryMemoryDao();
        }
        return countryMemoryDao;
    }

}