package edu.bbte.idde.ggim1816.backend.dao.jdbc;

import edu.bbte.idde.ggim1816.backend.dao.CountryDao;
import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;

public class JdbcDaoFactory extends DaoFactory {
    @Override
    public TourDao getTourDao() {
        return new TourJdbcDao();
    }

    @Override
    public CountryDao getCountryDao() {
        return new CountryJdbcDao();
    }
}
