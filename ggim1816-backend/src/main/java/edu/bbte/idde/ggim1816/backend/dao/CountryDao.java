package edu.bbte.idde.ggim1816.backend.dao;

import edu.bbte.idde.ggim1816.backend.model.Country;

public interface CountryDao extends Dao<Country> {

}
