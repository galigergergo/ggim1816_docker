package edu.bbte.idde.ggim1816.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigLoader {

    private static AppConfig appConfig = new AppConfig();

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigLoader.class);

    public static AppConfig get() {
        String configFileName = getConfigFileName();
        try {
            InputStream configStream = AppConfig.class.getResourceAsStream(configFileName);
            appConfig = new ObjectMapper().readValue(configStream, AppConfig.class);
            LOGGER.info("Read app config from {}: {}", configFileName, appConfig);
        } catch (IOException e) {
            LOGGER.error("Could not load config from {}, regressing", configFileName, e);
        }
        LOGGER.info("Final application config: {}", appConfig);
        return appConfig;
    }

    private static String getConfigFileName() {
        final StringBuilder nameBuilder = new StringBuilder("/application");
        String profile = System.getenv("PROFILE");
        if (profile == null || profile.isEmpty()) {
            LOGGER.info("Using default (unnamed) profile...");
        } else {
            LOGGER.info("Detected profile {}", profile);
            nameBuilder.append('-').append(profile);
        }
        return nameBuilder.append(".json").toString();
    }
}
