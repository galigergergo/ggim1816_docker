package edu.bbte.idde.ggim1816.backend.dao;

import edu.bbte.idde.ggim1816.backend.model.Tour;

public interface TourDao  extends Dao<Tour> {

    void updateNameById(Integer id, String name);

}
