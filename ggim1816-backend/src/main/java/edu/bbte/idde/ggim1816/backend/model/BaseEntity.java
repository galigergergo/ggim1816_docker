package edu.bbte.idde.ggim1816.backend.model;

import java.io.Serializable;

public abstract class BaseEntity implements Serializable {
    protected Integer id;

    public BaseEntity() {
    }

    public BaseEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
