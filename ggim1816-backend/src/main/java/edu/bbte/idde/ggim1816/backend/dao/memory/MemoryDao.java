package edu.bbte.idde.ggim1816.backend.dao.memory;

import edu.bbte.idde.ggim1816.backend.dao.Dao;
import edu.bbte.idde.ggim1816.backend.model.BaseEntity;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class MemoryDao<T extends BaseEntity> implements Dao<T> {

    protected final Map<Integer, T> map = new ConcurrentHashMap<>();
    protected final AtomicInteger idCounter = new AtomicInteger();

    @Override
    public Collection<T> selectAll() {
        return map.values();
    }

    @Override
    public T selectById(Integer id) {
        return map.get(id);
    }

    @Override
    public void deleteById(Integer id) {
        map.remove(id);
    }
}
