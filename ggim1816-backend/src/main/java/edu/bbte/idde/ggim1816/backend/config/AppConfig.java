package edu.bbte.idde.ggim1816.backend.config;

import java.io.Serializable;

public class AppConfig implements Serializable {
    private String daoType = "mem";
    private String jdbcUrl;
    private String jdbcUsername;
    private String jdbcPassword;
    private String jdbcDriverClassName;
    private Integer jdbcMaximumPoolSize;

    public String getDaoType() {
        return daoType;
    }

    public void setDaoType(String daoType) {
        this.daoType = daoType;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getJdbcUsername() {
        return jdbcUsername;
    }

    public void setJdbcUsername(String jdbcUsername) {
        this.jdbcUsername = jdbcUsername;
    }

    public String getJdbcPassword() {
        return jdbcPassword;
    }

    public void setJdbcPassword(String jdbcPassword) {
        this.jdbcPassword = jdbcPassword;
    }

    public String getJdbcDriverClassName() {
        return jdbcDriverClassName;
    }

    public void setJdbcDriverClassName(String jdbcDriverClassName) {
        this.jdbcDriverClassName = jdbcDriverClassName;
    }

    public Integer getJdbcMaximumPoolSize() {
        return jdbcMaximumPoolSize;
    }

    public void setJdbcMaximumPoolSize(Integer jdbcMaximumPoolSize) {
        this.jdbcMaximumPoolSize = jdbcMaximumPoolSize;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AppConfig{");
        sb.append("daoType='").append(daoType).append('\'');
        sb.append(", jdbcUrl='").append(jdbcUrl).append('\'');
        sb.append(", jdbcUsername='").append(jdbcUsername).append('\'');
        sb.append(", jdbcPassword='").append(jdbcPassword).append('\'');
        sb.append(", jdbcDriverClassName='").append(jdbcDriverClassName).append('\'');
        sb.append(", jdbcMaximumPoolSize=").append(jdbcMaximumPoolSize);
        sb.append('}');
        return sb.toString();
    }
}
