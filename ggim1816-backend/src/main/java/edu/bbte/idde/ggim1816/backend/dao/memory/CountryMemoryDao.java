package edu.bbte.idde.ggim1816.backend.dao.memory;

import edu.bbte.idde.ggim1816.backend.dao.CountryDao;
import edu.bbte.idde.ggim1816.backend.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CountryMemoryDao extends MemoryDao<Country> implements CountryDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountryMemoryDao.class);

    @Override
    public int create(Country country) {
        LOGGER.info("Creating new country in memory.");
        int id = idCounter.getAndIncrement();
        country.setId(id);
        map.put(id, country);
        return id;
    }

    @Override
    public void updateById(Integer id, Country country) {
        LOGGER.info("Updating country with id: {} in memory.", id);
        if (country.getId().equals(id)) {
            map.remove(id);
            map.put(id, country);
        }
    }

}

