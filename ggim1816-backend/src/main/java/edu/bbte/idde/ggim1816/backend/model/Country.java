package edu.bbte.idde.ggim1816.backend.model;

public class Country extends BaseEntity {
    String name;
    Integer population;

    public Country() {
        super();
    }

    public Country(String name, Integer population) {
        super();
        this.name = name;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }
}
