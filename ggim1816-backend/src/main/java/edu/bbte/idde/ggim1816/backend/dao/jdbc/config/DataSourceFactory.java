package edu.bbte.idde.ggim1816.backend.dao.jdbc.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.ggim1816.backend.config.AppConfig;
import edu.bbte.idde.ggim1816.backend.config.ConfigLoader;

import java.sql.Connection;
import java.sql.SQLException;

public final class DataSourceFactory {

    private static HikariDataSource DATA_SOURCE;

    private DataSourceFactory() {

    }

    public static synchronized Connection getConnection() throws SQLException {
        if (DATA_SOURCE == null) {
            DATA_SOURCE = createDataSource();
        }
        return DATA_SOURCE.getConnection();
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        AppConfig appConfig = ConfigLoader.get();
        hikariConfig.setDriverClassName(appConfig.getJdbcDriverClassName());
        hikariConfig.setJdbcUrl(appConfig.getJdbcUrl());
        hikariConfig.setUsername(appConfig.getJdbcUsername());
        hikariConfig.setPassword(appConfig.getJdbcPassword());
        hikariConfig.setMaximumPoolSize(appConfig.getJdbcMaximumPoolSize());
        return new HikariDataSource(hikariConfig);
    }
}
