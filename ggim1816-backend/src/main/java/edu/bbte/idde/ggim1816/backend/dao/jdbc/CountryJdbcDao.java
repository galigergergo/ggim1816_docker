package edu.bbte.idde.ggim1816.backend.dao.jdbc;

import edu.bbte.idde.ggim1816.backend.dao.CountryDao;
import edu.bbte.idde.ggim1816.backend.dao.DaoException;
import edu.bbte.idde.ggim1816.backend.dao.jdbc.config.DataSourceFactory;
import edu.bbte.idde.ggim1816.backend.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CountryJdbcDao extends JdbcDao<Country> implements CountryDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountryJdbcDao.class);

    @Override
    public int create(Country country) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO countries (name, population) VALUES (?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
            prepareStatementFromEntity(statement, country);
            int affectedRows = statement.executeUpdate();
            if (affectedRows > 0) {
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    LOGGER.info("Created country with id: {} (JDBC)", generatedKeys.getInt(1));
                    return generatedKeys.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Could not create country! (JDBC)", e);
            throw new DaoException("Could not create country! (JDBC)", e);
        }
        return -1;
    }

    @Override
    public Collection<Country> selectAll() {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(
                    "SELECT id, name, population FROM countries");
            List<Country> results = new ArrayList<>();
            while (resultSet.next()) {
                results.add(convertResultSetRow(resultSet));
            }
            return results;
        } catch (SQLException e) {
            LOGGER.error("Could not find all countries! (JDBC)", e);
            throw new DaoException("Could not find all countries! (JDBC)", e);
        }
    }

    @Override
    public Country selectById(Integer id) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "SELECT id, name, population FROM countries WHERE id = ?;");
            statement.setInt(1, id);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return convertResultSetRow(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Could not find country by id! (JDBC)", e);
            throw new DaoException("Could not find country by id! (JDBC)", e);
        }
        return null;
    }

    @Override
    public void updateById(Integer id, Country country) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "UPDATE countries SET name = ?, population = ? WHERE id = ?;");
            prepareStatementFromEntity(statement, id, country);
            statement.executeUpdate();
            LOGGER.info("Updated country with id: {} (JDBC)", id);
        } catch (SQLException e) {
            LOGGER.error("Could not update country by id! (JDBC)", e);
            throw new DaoException("Could not update country by id! (JDBC)", e);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Connection connection = DataSourceFactory.getConnection()) {
            final PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM countries WHERE id = ?;");
            statement.setInt(1, id);
            statement.executeUpdate();
            LOGGER.info("Deleted country with id: {} (JDBC)", id);
        } catch (SQLException e) {
            LOGGER.error("Could not delete country by id! (JDBC)", e);
            throw new DaoException("Could not delete country by id! (JDBC)", e);
        }
    }

    private Country convertResultSetRow(ResultSet resultSet) throws SQLException {
        Country country = new Country();
        country.setId(resultSet.getInt("id"));
        country.setName(resultSet.getString("name"));
        country.setPopulation(resultSet.getInt("population"));
        return country;
    }

    private void prepareStatementFromEntity(PreparedStatement statement, Country country) throws SQLException {
        statement.setString(1, country.getName());
        statement.setInt(2, country.getPopulation());
    }

    private void prepareStatementFromEntity(PreparedStatement statement, Integer id, Country country)
            throws SQLException {
        statement.setString(1, country.getName());
        statement.setInt(2, country.getPopulation());
        statement.setInt(3, id);
    }
}
