package edu.bbte.idde.ggim1816.backend.dao.memory;

import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;
import edu.bbte.idde.ggim1816.backend.model.Tour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TourMemoryDao extends MemoryDao<Tour> implements TourDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(TourMemoryDao.class);

    @Override
    public int create(Tour tour) {
        LOGGER.info("Creating new tour in memory.");
        DaoFactory.getInstance().getCountryDao().create(tour.getCountry());
        int id = idCounter.getAndIncrement();
        tour.setId(id);
        map.put(id, tour);
        return id;
    }

    @Override
    public void updateById(Integer id, Tour tour) {
        LOGGER.info("Updating tour with id: {} in memory.", id);
        DaoFactory.getInstance().getCountryDao().updateById(tour.getCountry().getId(), tour.getCountry());
        if (tour.getId().equals(id)) {
            map.remove(id);
            map.put(id, tour);
        }
    }

    @Override
    public void updateNameById(Integer id, String name) {
        LOGGER.info("Updating name of tour with id: {} in memory.", id);
        map.get(id).setName(name);
    }

}
