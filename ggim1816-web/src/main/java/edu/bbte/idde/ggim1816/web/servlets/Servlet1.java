package edu.bbte.idde.ggim1816.web.servlets;

import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;
import edu.bbte.idde.ggim1816.backend.model.Country;
import edu.bbte.idde.ggim1816.backend.model.Tour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet1",
        urlPatterns = {"/tours"})
public class Servlet1 extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(Servlet1.class);

    final TourDao tourDao = DaoFactory.getInstance().getTourDao();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing tours servlet");

        // fill dao if not yet filled by the other servlet
        if (tourDao.selectAll().isEmpty()) {
            tourDao.create(new Tour("Tour1", "northern", new Country("Romania", 19), 11.1, 3.5, 200, 30, 873));
            tourDao.create(new Tour("Tour2", "southern", new Country("France", 30), 12.0, 4.0, 257, 45, 1111));
            tourDao.create(new Tour("Tour3", "western", new Country("Germany", 40), 1.1, 3.5, 200, 243, 675));
            tourDao.create(new Tour("Tour4", "northern", new Country("Norway", 24), 5.1, 2.0, 333, 111, 435));
            tourDao.create(new Tour("Tour5", "central", new Country("India", 1000), 11.6, 4.0, 355, 145, 866));
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        LOG.info("Destroying tours servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info("Request arrived to tours servlet");

        PrintWriter writer = resp.getWriter();
        String id = req.getParameter("id");
        if (id == null) {
            LOG.info("Requested all tours");
            for (Tour tour : tourDao.selectAll()) {
                writer.println(tour);
            }
        } else {
            Tour selectedTour = tourDao.selectById(Integer.parseInt(id));
            if (selectedTour == null) {
                LOG.info("Requested tour not found");
                resp.setStatus(404);
                writer.println("Tour not found!");
            } else {
                LOG.info("Requested tour by id: {}", id);
                writer.println(selectedTour);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info("Request arrived to tours servlet");

        PrintWriter writer = resp.getWriter();

        // get parameters
        String name = req.getParameter("name");
        String region = req.getParameter("region");
        String country = req.getParameter("country");
        String population = req.getParameter("population");
        String distance = req.getParameter("distance");
        String duration = req.getParameter("duration");
        String ascent = req.getParameter("ascent");
        String descent = req.getParameter("descent");
        String peak = req.getParameter("peak");

        // check for numbers and if all the parameters were given
        if (name == null || region == null) {
            resp.setStatus(400);
            writer.write("Bad Request");
        } else {
            try {
                Integer pop = Integer.parseInt(population);
                Double dist = Double.parseDouble(distance);
                Double dur = Double.parseDouble(duration);
                Integer asc = Integer.parseInt(ascent);
                Integer desc = Integer.parseInt(descent);
                Integer pea = Integer.parseInt(peak);

                tourDao.create(new Tour(name, region, new Country(country, pop), dist, dur, asc, desc, pea));

                resp.sendRedirect(req.getContextPath() + "/main");
            } catch (NumberFormatException e) {
                resp.setStatus(400);
                writer.write("Bad Request");
            }
        }
    }
}