package edu.bbte.idde.ggim1816.web.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        urlPatterns = {
            "/login"
        },
        initParams = {
            @WebInitParam(name = "username", value = "user"),
            @WebInitParam(name = "password", value = "pass")
        }
)
public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/login.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Verifying authentication...");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || password == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        } else if (username.equals(this.getServletConfig().getInitParameter("username"))
                && password.equals(this.getServletConfig().getInitParameter("password"))) {
            req.getSession().setAttribute("authenticated", true);
            resp.sendRedirect(req.getContextPath() + "/main");
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.setStatus(401);
            resp.getWriter().println("Incorrect user name or password!");
        }
    }
}