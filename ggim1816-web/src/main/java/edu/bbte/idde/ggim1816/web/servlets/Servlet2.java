package edu.bbte.idde.ggim1816.web.servlets;

import com.github.jknack.handlebars.Template;
import edu.bbte.idde.ggim1816.backend.dao.DaoFactory;
import edu.bbte.idde.ggim1816.backend.dao.TourDao;
import edu.bbte.idde.ggim1816.backend.model.Country;
import edu.bbte.idde.ggim1816.backend.model.Tour;
import edu.bbte.idde.ggim1816.web.handlebars.HandlebarsTemplateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/main")
public class Servlet2 extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(Servlet2.class);

    final TourDao tourDao = DaoFactory.getInstance().getTourDao();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing tours main page servlet");

        // fill dao if not yet filled by the other servlet
        if (tourDao.selectAll().isEmpty()) {
            tourDao.create(new Tour("Tour1", "northern", new Country("Romania", 19), 11.1, 3.5, 200, 30, 873));
            tourDao.create(new Tour("Tour2", "southern", new Country("France", 30), 12.0, 4.0, 257, 45, 1111));
            tourDao.create(new Tour("Tour3", "western", new Country("Germany", 40), 1.1, 3.5, 200, 243, 675));
            tourDao.create(new Tour("Tour4", "northern", new Country("Norway", 24), 5.1, 2.0, 333, 111, 435));
            tourDao.create(new Tour("Tour5", "central", new Country("India", 1000), 11.6, 4.0, 355, 145, 866));
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        LOG.info("Destroying tours main page servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Request arrived to tours main page servlet");

        // fill model with available tours
        Map<String, Object> model = new ConcurrentHashMap<>();
        List<Tour> tours = new ArrayList<>();
        tours.addAll(tourDao.selectAll());
        model.put("tours", tours);

        // rendering
        Template template = HandlebarsTemplateFactory.getTemplate("index");
        template.apply(model, resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().invalidate();
        resp.sendRedirect(req.getContextPath() + "/main");
    }
}