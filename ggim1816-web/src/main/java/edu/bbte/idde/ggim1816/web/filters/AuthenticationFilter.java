package edu.bbte.idde.ggim1816.web.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "AuthenticationFilter",
        urlPatterns = {"/main"})
public class AuthenticationFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("Initializing user filter");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        LOGGER.info("Filter is checking user authentication");

        HttpServletResponse httpRes = null;
        if (res instanceof HttpServletResponse) {
            httpRes = (HttpServletResponse) res;
        }
        HttpServletRequest httpReq = (HttpServletRequest) req;

        HttpSession session = httpReq.getSession();
        if (session != null && session.getAttribute("authenticated") != null
                && (boolean) session.getAttribute("authenticated")) {
            chain.doFilter(req, res);
        } else {
            httpRes.sendRedirect(httpReq.getContextPath() + "/login");
        }
    }

    @Override
    public void destroy() {
        LOGGER.info("Destroying user filter");
    }
}